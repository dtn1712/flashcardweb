<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'flashcard');

/** MySQL database username */
define('DB_USER', 'dtn1712');

/** MySQL database password */
define('DB_PASSWORD', '123456');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',nu{^AyCy./%X?=_; {dugZ]|vlpfEnw}Zbq|Y~1nolV5htwM,6MY&u:U:8(Xp;!');
define('SECURE_AUTH_KEY',  '*xH3?= qMWb|Uih#*7(:kX-8GLe%>2RkEW;20:MEceA,%0WT*do`(O<[pK }oC<~');
define('LOGGED_IN_KEY',    '>50Cj]>!3Z?]Sp{|GjXW?=~IdN-wPM>u!V9~H4axVS,K67BD;zALunl(t(N?z!)(');
define('NONCE_KEY',        '@uHa@Fk|5{ET-3kA@k`2snB9g2B/JW43+}63xFrxicyK#Fsxm#8L8Tg`2(4!&e.Z');
define('AUTH_SALT',        'h:]MW]g:&PDV?xEBF=e ?%6aS/%:9AiLdGWzEWX{TUEa.B^J [#/x`JpR(A=Y;3.');
define('SECURE_AUTH_SALT', '{sY7oL +hP%7Y!?<1R6h2W,8Kg?k{ c*XZ4<*<Hx=Lc,85Z[pWJ,uoFt-~n[zwgP');
define('LOGGED_IN_SALT',   'GYZ}wGIZ%5cjJxN`-WoH;O`z-Eoj4{)P^l:/c>%E^H+,fS5]S9M9vh#up;:Elc=A');
define('NONCE_SALT',       'Y$O@STJkcI_|@S=`>@%(q8lyNL6tw.SNM`|6%rPv7 yy}SZCPTt4Un/K0n;[5}C@');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'fvl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
